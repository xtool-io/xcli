package xtool.xcli.core.resolver

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component
import xtool.xcli.core.PackageResolver
import xtool.xcli.core.model.maven.JavaPackage

/**
 * PackageResolver que organiza os pacotes por camadas
 */
@Component
@Primary
@ConditionalOnProperty(name = ["package-resolver"], havingValue = "layer", matchIfMissing = true)
class LayerPackageResolver : PackageResolver {
    override fun resolve(key: String, cxt: MutableMap<String, Any>): JavaPackage = when (key) {
        "domain" -> JavaPackage("domain")
        else -> throw IllegalAccessException("Não foi possível localizar o pacote para a chave ${key}")
    }
}
