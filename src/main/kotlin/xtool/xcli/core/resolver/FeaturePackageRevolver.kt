package xtool.xcli.core.resolver

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import xtool.xcli.core.PackageResolver
import xtool.xcli.core.model.maven.JavaPackage
import xtool.xcli.core.model.plantuml.PlantClass

/**
 * PackageResolver que organiza os pacotes por feature
 */
@Component
@ConditionalOnProperty(name = ["package-resolver"], havingValue = "feature", matchIfMissing = false)
class FeaturePackageRevolver: PackageResolver {
    override fun resolve(key: String, ctx: MutableMap<String, Any>): JavaPackage = when(key) {
        "domain" -> JavaPackage("""${(ctx["plantClass"] as PlantClass).name.decapitalize()}.domain""")
        else -> throw IllegalAccessException("Não foi possível localizar o pacote para a chave ${key}")
    }
}
