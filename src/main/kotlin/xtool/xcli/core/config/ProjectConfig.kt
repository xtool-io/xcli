package xtool.xcli.core.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import xtool.xcli.core.model.Project
import java.nio.file.Paths

@Configuration
class ProjectConfig(private val env: Environment) {

    @Bean
    fun getProject(): Project {
        return Project(Paths.get(env.getRequiredProperty("project.path")))
    }

}
