package xtool.xcli.core.config

import org.jline.utils.AttributedString
import org.jline.utils.AttributedStyle
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.shell.jline.PromptProvider
import xtool.xcli.core.model.Project

@Configuration
class PromptConfig(private val project: Project) {

    @Bean
    fun promptProvider(): PromptProvider? {
        val prompt = String.format("xtool@%s ~ ", project.path.fileName.toString())
        return PromptProvider {
            AttributedString(
                prompt,
                AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.BLUE)
            )
        }
    }
}
