package xtool.xcli.core.ext

inline class JavaType(val value: String)
inline class JavaAnnotationType(val value: String)
inline class JavaImport(val value: String = "")
