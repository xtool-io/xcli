package xtool.xcli.core.ext

import org.jboss.forge.roaster.model.source.AnnotationSource
import org.jboss.forge.roaster.model.source.FieldSource
import org.jboss.forge.roaster.model.source.JavaClassSource
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.PathMatcher
import kotlin.streams.asSequence

/**
 * Lista diretórios e serem ignorados na busca
 */
private val ignoreFiles: Collection<PathMatcher> = listOf(
    FileSystems.getDefault().getPathMatcher("glob:**/resources/BOOT-INF/*"),
    FileSystems.getDefault().getPathMatcher("glob:**/.vscode"),
    FileSystems.getDefault().getPathMatcher("glob:**/.vscode/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/target"),
    FileSystems.getDefault().getPathMatcher("glob:**/target/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/.git"),
    FileSystems.getDefault().getPathMatcher("glob:**/.git/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/.k8s"),
    FileSystems.getDefault().getPathMatcher("glob:**/.k8s/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/.idea"),
    FileSystems.getDefault().getPathMatcher("glob:**/.idea/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/.xtool"),
    FileSystems.getDefault().getPathMatcher("glob:**/.xtool/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/docker"),
    FileSystems.getDefault().getPathMatcher("glob:**/docker/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/*-frontend/node"),
    FileSystems.getDefault().getPathMatcher("glob:**/*-frontend/node/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/*-frontend/node_modules"),
    FileSystems.getDefault().getPathMatcher("glob:**/*-frontend/node_modules/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/*-frontend/dist"),
    FileSystems.getDefault().getPathMatcher("glob:**/*-frontend/dist/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/*-backend/src/main/java/**/config/**"),
    FileSystems.getDefault().getPathMatcher("glob:**/*-backend/src/main/java/**/core/**")
)

/**
 * Busca por todos os arquivos nos subdiretórios.
 */
fun Path.findFiles(): List<Path> {
    return Files.walk(this)
        .asSequence()
        .filter(Files::isRegularFile)
        .filter { p1 -> ignoreFiles.none { p2 -> p2.matches(p1) } }
        .toList();
}

fun JavaClassSource.updateAnnotation(
    javaAnnotationType: JavaAnnotationType,
    javaImport: JavaImport
): AnnotationSource<JavaClassSource> {
    if (!this.hasAnnotation(javaAnnotationType.value)) {
        this.addImport(javaImport.value)
        return this.addAnnotation(javaAnnotationType.value)
    }
    return this.getAnnotation(javaAnnotationType.value)
}

fun FieldSource<JavaClassSource>.updateAnnotation(
    javaAnnotationType: JavaAnnotationType,
    javaImport: JavaImport
): AnnotationSource<JavaClassSource> {
    if (!this.hasAnnotation(javaAnnotationType.value)) {
        this.origin.addImport(javaImport.value)
        return this.addAnnotation(javaAnnotationType.value)
    }
    return this.getAnnotation(javaAnnotationType.value)
}

fun JavaClassSource.updateField(
    name: String,
    javaType: Pair<JavaType, JavaImport> = Pair(JavaType("String"), JavaImport())
): FieldSource<JavaClassSource> {
    if (!this.hasField(name)) {
        if (javaType.second.value.isNotEmpty()) {
            this.addImport(javaType.second.value)
        }
        return this.addField()
            .setName(name)
            .setType(javaType.first.value)
    }
    return this.getField(name)
}


