package xtool.xcli.core.impl

import org.jboss.forge.roaster.Roaster
import org.jboss.forge.roaster.model.source.JavaClassSource
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import xtool.xcli.core.FS
import xtool.xcli.core.model.Project
import xtool.xcli.core.model.maven.JavaPackage
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.name

@Component
class FSImpl(val project: Project) : FS {

    override fun createFile(path: String, content: String) {
        val finalPath: Path = project.path.resolve(path)
        if (Files.exists(finalPath)) {
            println("SKIP - ${path}")
            return;
        }
        write(finalPath, content)
        println("CREATE - ${path}")
    }

    override fun createClass(javaPackage: JavaPackage, javaClassSource: (JavaClassSource) -> Unit) {
        val javaClass = Roaster.create(JavaClassSource::class.java)
        javaClass.setPackage(project.rootPackage.resolve(javaPackage.name).name)
        javaClassSource(javaClass)
        write(
            project.mainSourceFolder
                .resolve(project.rootPackage.dir)
                .resolve(javaPackage.dir)
                .resolve("${javaClass.name}.java"),
            javaClass.toUnformattedString()
        )
    }

    override fun createClass(javaPackage: JavaPackage, className: String) {
        val javaClass = Roaster.create(JavaClassSource::class.java)
        javaClass.setPackage(project.rootPackage.resolve(javaPackage.name).name)
        javaClass.name = className
        write(
            project.mainSourceFolder
                .resolve(project.rootPackage.dir)
                .resolve(javaPackage.dir)
                .resolve("${className}.java"),
            javaClass.toUnformattedString()
        )
    }

    override fun updateClass(name: String, javaClassSource: (JavaClassSource) -> Unit) {
        project.javaClasses.single { it.name == name }.let {
            javaClassSource(it)
            write(
                project.mainSourceFolder
                    .resolve(JavaPackage(it.`package`).dir)
                    .resolve("${it.name}.java"),
                Roaster.format(it.toUnformattedString())
            )
        }
    }

    private fun write(path: Path, content: String) {
        if (Files.notExists(path.parent)) Files.createDirectories(path.parent)
        Files.newBufferedWriter(path).use { writer ->
            writer.write(content)
            writer.flush()
//            System.out.println(
//                ansi().bold().fg(Ansi.Color.GREEN).a("[CREATE FILE   ] ").reset().a(relativePath)
//            )
        }
    }
}
