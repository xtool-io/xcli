package xtool.xcli.core.schematic

import xtool.xcli.core.FS

fun interface Rule {
    fun apply(fs: FS): FS
}
