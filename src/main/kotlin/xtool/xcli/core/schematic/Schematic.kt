package xtool.xcli.core.schematic

import xtool.xcli.core.FS

abstract class Schematic() {

    abstract fun run(fs: FS, ctx: MutableMap<String, Any>)
}
