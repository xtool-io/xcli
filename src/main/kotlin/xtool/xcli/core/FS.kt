package xtool.xcli.core

import org.jboss.forge.roaster.model.source.JavaClassSource
import xtool.xcli.core.model.maven.JavaPackage

/**
 * API para operar o sistema de arquivos do projeto.
 */
interface FS {

    /**
     * Cria um arquivo no projeto.
     */
    fun createFile(path: String, content: String)

    /**
     * Cria uma classe java. A classe será criada a partir do source folder src/main/java e na raiz do root package do projeto
     */
    fun createClass(javaPackage: JavaPackage, className: String)

    /**
     *
     */
    fun createClass(javaPackage: JavaPackage, javaClassSource: (JavaClassSource) -> Unit)

    /**
     * Atualiza uma classe java
     */
    fun updateClass(name: String, javaClassSource: (JavaClassSource) -> Unit)
}
