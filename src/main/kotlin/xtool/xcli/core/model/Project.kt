package xtool.xcli.core.model

import org.jboss.forge.roaster.Roaster
import org.jboss.forge.roaster.model.JavaUnit
import org.jboss.forge.roaster.model.source.JavaClassSource
import org.jboss.forge.roaster.model.source.JavaInterfaceSource
import org.jboss.forge.roaster.model.source.JavaSource
import xtool.xcli.core.ext.findFiles
import xtool.xcli.core.model.maven.JavaPackage
import xtool.xcli.core.model.maven.PomXml
import xtool.xcli.core.model.plantuml.PlantClassDiagram
import java.nio.file.Files
import java.nio.file.Path

/**
 * Classe que representa um projeto Maven/SpringBoot
 */
class Project(val path: Path) {


    /**
     * Retorna o nome do projeto
     */
    val name: String
        get() = this.pomXml.artifactId


    /**
     * Retorna o package do projeto
     */
    val rootPackage: JavaPackage
        get() = JavaPackage(this.pomXml.groupId)

    /**
     * Retorna todos os arquivos plantuml do diretório src/diagrams
     */
    val plantClassDiagrams: List<PlantClassDiagram>
        get() = path.findFiles()
            .filter { it.toFile().toString().endsWith(".plantuml") }
            .map { PlantClassDiagram.of(it, this) }
            .toList()

    /**
     * Retorna o diagrama plantUML de domínio do projeto
     */
    val domainDiagram: PlantClassDiagram?
        get() = this.plantClassDiagrams.find { it.path.toFile().toString().endsWith("domain.plantuml") }

    /**
     * Retorna todas as classes java do projeto
     */
    val javaClasses: List<JavaClassSource>
        get() = this.javaUnits.asSequence()
            .filter { it.getGoverningType<JavaSource<*>>().isClass }
            .map { it.getGoverningType<JavaClassSource>() }
            .toList()

    /**
     * Retorna todas as entidades JPA do projeto
     */
    val jpaClasses: List<JavaClassSource>
        get() = this.javaClasses.asSequence()
            .filter { it.hasAnnotation("Entity") }
            .toList()

    /**
     * Retorna todas as interfaces de repositório do projeto.
     */
    val repositories: List<JavaInterfaceSource>
        get() = this.javaUnits.asSequence()
            .filter { it.getGoverningType<JavaSource<*>>().isInterface() }
            .map { it.getGoverningType<JavaInterfaceSource>() }
            .toList()

    /**
     * Retorna todas as classes Rest do projeto
     */
    val rests: List<JavaClassSource>
        get() = this.javaClasses.asSequence()
            .filter { it.hasAnnotation("RestController") }
            .toList()

    /**
     * Retorna a representação do arquivo pom.xml do projeto
     */
    val pomXml: PomXml
        get() = PomXml(this, this.path.resolve("pom.xml"))

    /**
     * Retorna o source folder src/main/java
     */
    val mainSourceFolder: Path
        get() {
            if (pomXml.isMultiModule) {
                return this.path.resolve("${name}-backend/src/main/java")
            }
            return this.path.resolve("src/main/java")
        }


    private val javaUnits: List<JavaUnit>
        get() = this.path.findFiles()
            .filter { it.toFile().toString().endsWith(".java") }
            .map { parseJavaUnit(it) }
            .toList()

    private fun parseJavaUnit(path: Path): JavaUnit {
        return Roaster.parseUnit(Files.newInputStream(path))
    }
}
