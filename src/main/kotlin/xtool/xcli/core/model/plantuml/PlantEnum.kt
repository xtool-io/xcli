package xtool.xcli.core.model.plantuml

import net.sourceforge.plantuml.cucadiagram.ILeaf

/**
 * Classe que representa um Enum
 */
class PlantEnum(private val leaf: ILeaf) {

    /**
     * Retorna o nome do Enum
     */
    val name: String
        get() = leaf.display.asStringWithHiddenNewLine()


    /**
     * Retorna os valores do Enum
     */
    val values: List<String>
        get() = leaf.bodier.fieldsToDisplay.asSequence()
            .filter { it.getDisplay(false).isNotEmpty() }
            .map { it.getDisplay(false) }
            .toList()

}
