package xtool.xcli.core.model.plantuml

import net.sourceforge.plantuml.SourceStringReader
import net.sourceforge.plantuml.classdiagram.ClassDiagram
import net.sourceforge.plantuml.cucadiagram.LeafType
import xtool.xcli.core.model.Project
import java.nio.file.Files
import java.nio.file.Path

/**
 * Classe que representa um diagrama de classe PlantUML
 */
data class PlantClassDiagram internal constructor(
    val path: Path,
    val project: Project,
    val classDiagram: ClassDiagram
) {

    companion object {
        //Método factory para criação de um objeto PlantClassDiagram
        fun of(path: Path, project: Project): PlantClassDiagram {
            require(!Files.notExists(path)) { "Diagrama de classe não encontrado em $path" }
            val diagram = String(Files.readAllBytes(path))
            val reader = SourceStringReader(diagram)
            return reader.blocks.asSequence()
                .map { it.diagram }
                .filter { it is ClassDiagram }
                .map { it as ClassDiagram }
                .map { PlantClassDiagram(path, project, it) }
                .first()
        }
    }

    /**
     * Retorna a lista de classes do diagrama
     */
    val classes: List<PlantClass>
        get() = this.classDiagram.getGroups(false).asSequence()
            .flatMap { it.leafsDirect.asSequence() }
            .filter { it.entityType.equals(LeafType.CLASS) }
            .map { PlantClass(this, it) }
            .toList()


    /**
     * Retorna a lista de relacionamentos do diagrama
     */
    val relationships: List<PlantRelationship>
        get() = classDiagram.entityFactory.links.asSequence()
            .filter { !it.type.isInvisible }
            .filter { it.entity1.entityType == LeafType.CLASS }
            .filter { it.entity2.entityType == LeafType.CLASS }
            .map { PlantRelationship(this, it) }
            .toList()

}
