package xtool.xcli.core.model.plantuml

import net.sourceforge.plantuml.cucadiagram.Member
import org.apache.commons.lang3.StringUtils
import strman.Strman
import xtool.xcli.core.ext.JavaImport
import xtool.xcli.core.ext.JavaType
import xtool.xcli.core.model.Project

class PlantField(
    val plancClass: PlantClass,
    val member: Member
) {

    /**
     * Retorna o nome do atributo da classe PlantUML
     */
    val name: String
        get() = memberName()

    /**
     * Retorna o tipo do atributo da classe PlantUML
     */
    val rawType: String
        get() {
            if (this.hasMultiplicity()) {
                return StringUtils.substring(memberType(), 0, StringUtils.indexOf(memberType(), "[")).trim()
            }
            return if (this.hasProperties()) {
                StringUtils.substring(memberType(), 0, StringUtils.indexOf(memberType(), "{")).trim()
            } else memberType()
        }

    /**
     * Retorna o tipo java com o par (javaType e JavaImport)
     */
    val javaType: Pair<JavaType, JavaImport>
        get() = when (rawType) {
            "Long" -> Pair(JavaType("Long"), JavaImport())
            "Integer" -> Pair(JavaType("Integer"), JavaImport())
            "Short" -> Pair(JavaType("Short"), JavaImport())
            "Boolean" -> Pair(JavaType("Boolean"), JavaImport())
            "LocalDate" -> Pair(JavaType("LocalDate"), JavaImport("java.time.LocalDate"))
            "LocalDateTime" -> Pair(JavaType("LocalDateTime"), JavaImport("java.time.LocalDateTime"))
            "BigDecimal" -> Pair(JavaType("BigDecimal"), JavaImport("java.math.BigDecimal"))
            else -> Pair(JavaType("String"), JavaImport())
        }

    private fun hasMultiplicity(): Boolean {
        return Strman.containsAll(memberType(), arrayOf("[", "]"))
    }

    private fun hasProperties(): Boolean {
        return Strman.containsAll(memberType(), arrayOf("{", "}"))
    }

    private fun memberName(): String {
        return member.getDisplay(false).split(":".toRegex()).toTypedArray().get(0).trim { it <= ' ' }
    }

    private fun memberType(): String {
        return member.getDisplay(false).split(":".toRegex()).toTypedArray().get(1).trim { it <= ' ' }
    }
}
