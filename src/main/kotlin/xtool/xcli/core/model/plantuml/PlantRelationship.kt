package xtool.xcli.core.model.plantuml

import net.sourceforge.plantuml.cucadiagram.Link
import net.sourceforge.plantuml.cucadiagram.LinkDecor
import org.jboss.forge.roaster.model.source.JavaClassSource
import xtool.xcli.core.model.Project
import java.util.*

/**
 * Classe que representa um relacionamento no diagrama de classe
 */
class PlantRelationship(
    val plantClassDiagram: PlantClassDiagram,
    val link: Link,
    private val rel: Pair<Linkable, Linkable> = Pair(Left(plantClassDiagram.project, link), Right(plantClassDiagram.project, link))
) {

    val left: Linkable
        get() = this.rel.first


    val right: Linkable
        get() = this.rel.second

    val isAssociation: Boolean
        get() = (left.decor == LinkDecor.ARROW || left.decor == LinkDecor.NONE) &&
                (right.decor == LinkDecor.ARROW || right.decor == LinkDecor.NONE)

    val isComposition: Boolean
        get() = (left.decor == LinkDecor.COMPOSITION) xor (right.decor == LinkDecor.COMPOSITION)

    val isUnidirectional: Boolean
        get() = left.decor == LinkDecor.ARROW && right.decor == LinkDecor.NONE ||
                left.decor == LinkDecor.NONE && right.decor == LinkDecor.ARROW

    val isOneToMany: Boolean
        get() = left.isToMany && right.isToOne

    val isManyToMany: Boolean
        get() = left.isToMany && right.isToMany


    val isManyToOne: Boolean
        get() = left.isToOne && right.isToMany


    val isOneToOne: Boolean
        get() = left.isToOne && right.isToOne


    interface Linkable {
        val name: String
        val multiplicity: String
        val decor: LinkDecor
        val isToOne: Boolean
        val isToMany: Boolean
        val isNavigable: Boolean
        val javaClass: JavaClassSource
    }

    class Left(private val project: Project, private val link: Link) : Linkable {
        override val name: String
            get() = link.entity1.display.asStringWithHiddenNewLine()
        override val multiplicity: String
            get() = link.qualifier2.trim { it <= ' ' }
        override val decor: LinkDecor
            get() = link.type.decor2
        override val isToOne: Boolean
            get() = Arrays.asList("0..1", "1").contains(multiplicity)
        override val isToMany: Boolean
            get() = Arrays.asList("*", "0..*", "1..*").contains(multiplicity)
        override val isNavigable: Boolean
            get() = link.type.decor2 == LinkDecor.ARROW
        override val javaClass: JavaClassSource
            get() = project.javaClasses.asSequence()
                .filter { javaClass -> javaClass.name.equals(name) }
                .first()
    }

    class Right(private val project: Project, private val link: Link) : Linkable {
        override val name: String
            get() = link.entity2.display.asStringWithHiddenNewLine()
        override val multiplicity: String
            get() = link.qualifier1.trim { it <= ' ' }
        override val decor: LinkDecor
            get() = link.type.decor1
        override val isToOne: Boolean
            get() = Arrays.asList("0..1", "1").contains(multiplicity)
        override val isToMany: Boolean
            get() = Arrays.asList("*", "0..*", "1..*").contains(multiplicity)
        override val isNavigable: Boolean
            get() = link.type.decor1 == LinkDecor.ARROW
        override val javaClass: JavaClassSource
            get() = project.javaClasses.asSequence()
                .filter { javaClass -> javaClass.name.equals(name) }
                .first()
    }

    override fun toString(): String {
        return "PlantRelationship(source=${left.name}[${left.multiplicity}]  [${right.multiplicity}]${right.name})"
    }


}
