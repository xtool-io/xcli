package xtool.xcli.core.model.plantuml

import net.sourceforge.plantuml.classdiagram.ClassDiagram
import net.sourceforge.plantuml.cucadiagram.ILeaf
import net.sourceforge.plantuml.skin.VisibilityModifier
import strman.Strman
import xtool.xcli.core.model.Project
import java.util.*

/**
 * Classe que representa uma classe do diagrama PlantUML.
 */
data class PlantClass(
    private val plantClassDiagram: PlantClassDiagram,
    val leaf: ILeaf
) {
    /**
     * Retorna o nome da classe PlantUML
     */
    val name: String
        get() = leaf.display.asStringWithHiddenNewLine()

    /**
     * Retorna o nome do pacote
     */
    val packageName: String?
        get() = plantClassDiagram.classDiagram.getGroups(false).first()?.display?.asStringWithHiddenNewLine();

    /**
     * Retorna a lista de atributos da classe
     */
    val attributes: List<PlantField>
        get() = this.leaf.bodier.fieldsToDisplay.asSequence()
            .filter { it.getDisplay(false).isNotEmpty() }
            .filter { Objects.nonNull(it.visibilityModifier) }
            .filter { it.visibilityModifier.equals(VisibilityModifier.PRIVATE_FIELD) }
            .map { PlantField(this, it) }
            .toList()

    /**
     * Retorna a lista de estereotipos da classe
     */
    val stereotypes: List<String>
        get() = leaf.stereotype.getLabels(false).asSequence()
            .map { Strman.between(it, "<<", ">>") }
            .map { it.joinToString() }
            .toList()


    /**
     * Retorna todos os relacionamento com a classe atual
     */
    val relationships: List<PlantRelationship>
        get() {
            val rels = mutableListOf<PlantRelationship>()
            plantClassDiagram.relationships.asSequence()
                .filter { it.left.name == this.name }
                .map { PlantRelationship(this.plantClassDiagram, it.link, Pair(it.left, it.right)) }
                .forEach { rels.add(it) }
            plantClassDiagram.relationships.asSequence()
                .filter { it.right.name == this.name }
                .map { PlantRelationship(this.plantClassDiagram, it.link, Pair(it.right, it.left)) }
                .forEach { rels.add(it) }
            return rels
        }

}
