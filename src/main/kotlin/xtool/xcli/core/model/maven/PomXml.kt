package xtool.xcli.core.model.maven

import org.jdom2.Element
import org.jdom2.Namespace
import org.jdom2.input.SAXBuilder
import xtool.xcli.core.model.Project
import java.nio.file.Files
import java.nio.file.Path
import java.util.*

/**
 * Classe que representa um o arquivo pom.xml
 */
class PomXml(
    val project: Project,
    private val path: Path) {

    private val namespace = Namespace.getNamespace("http://maven.apache.org/POM/4.0.0")

    private var pomDoc = SAXBuilder().build(Files.newBufferedReader(path))

    /**
     * Retorna a String da tag artifactId
     */
    val artifactId: String
        get() = pomDoc.rootElement.getChild("artifactId", namespace).text


    /**
     * Retorna a String da tag groupId
     */
    val groupId: String
        get() = pomDoc.rootElement.getChild("groupId", namespace).text


    /**
     * Retorna a String da tag version
     */
    val version: String
        get() = pomDoc.rootElement.getChild("version", namespace).text


    /**
     * Propriedade com a lista de todas as dependências do arquivo pom.xml
     */
    val dependencies: List<PomDependency>
        get() {
            val dependencies: MutableList<PomDependency> = ArrayList()
            val dependenciesNode: Element = this.pomDoc.rootElement.getChild("dependencies", namespace)
            for (dependency in dependenciesNode.children) {
                val groupId = dependency.getChild("groupId", namespace).textTrim
                val artifactId = dependency.getChild("artifactId", namespace).textTrim
                if (Objects.nonNull(dependency.getChild("version", namespace))) {
                    val version = dependency.getChild("version", namespace).textTrim
                    dependencies.add(PomDependency(groupId, artifactId, version))
                    continue
                }
                dependencies.add(PomDependency(groupId, artifactId))
            }
            return dependencies
        }

    /**
     * Verifica se o projeto é multimodulo
     */
    val isMultiModule: Boolean
        get() = Objects.nonNull(pomDoc.rootElement.getChild("modules", namespace))

}
