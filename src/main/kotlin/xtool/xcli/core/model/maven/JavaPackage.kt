package xtool.xcli.core.model.maven

import java.nio.file.Paths

/**
 * Classe que representa um pacote java
 */
data class JavaPackage(val name: String) {

    val dir: String
        get() = Paths.get(name).toFile().toString().replace(".", "/")

    fun resolve(newJavaPackage: String): JavaPackage {
        val resolve = Paths.get(this.dir).resolve(newJavaPackage.replace(".", "/"))
        return JavaPackage(resolve.toString().replace("/", "."))
    }

}
