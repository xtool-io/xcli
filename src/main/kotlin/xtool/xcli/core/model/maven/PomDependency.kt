package xtool.xcli.core.model.maven

/**
 * Classe que representa uma dependencia do pom xml.
 */
class PomDependency(
    val groupId: String,
    val artifactId: String,
    val version: String = ""
) {
}
