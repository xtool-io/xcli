package xtool.xcli.core

import xtool.xcli.core.model.maven.JavaPackage

interface PackageResolver {
    fun resolve(key: String, cxt: MutableMap<String, Any>): JavaPackage
}
