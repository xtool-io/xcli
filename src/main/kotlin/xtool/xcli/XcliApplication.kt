package xtool.xcli

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class XcliApplication

const val XTOOL_COMMAND_GROUP = "Xtool Commands"

fun main(args: Array<String>) {
    runApplication<XcliApplication>(*args)
}
