package xtool.xcli.service

import org.springframework.stereotype.Service
import xtool.xcli.core.model.Project

@Service
class ProjectInfoService(private val project: Project) {

    fun run() {
        println("Nome: ${project.name}")
        println("Root Package: ${project.rootPackage.name}")
        println("")
        println("--- Diagramas: ${project.plantClassDiagrams.size} ---")
        project.plantClassDiagrams.forEach { println(project.path.relativize(it.path)) }
        println("")
        println("--- Classes JPA: ${project.jpaClasses.size} ---")
        project.jpaClasses.forEach { println(it.qualifiedName) }
        println("")
        println("--- Repositórios: ${project.repositories.size} ---")
        project.repositories.forEach { println(it.qualifiedName) }
        println("")
        println("--- Rests: ${project.rests.size} ---")
        project.rests.forEach { println(it.qualifiedName) }
    }
}
