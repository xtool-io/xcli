package xtool.xcli.service

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import xtool.xcli.core.FS
import xtool.xcli.core.PackageResolver
import xtool.xcli.core.model.plantuml.PlantClassDiagram
import xtool.xcli.core.schematic.Schematic

/**
 * Classe de service que realiza o mapeamento JPA do diagrama de classe.
 */
@Service
class EntityMapService(
    val fs: FS,
    val packageResolver: PackageResolver,
    @Qualifier("EntityMapClassSchematic") val entityMapClassSchematic: Schematic,
    @Qualifier("EntityMapLongAttrSchematic") val entityMapLongAttrSchematic: Schematic,
    @Qualifier("EntityMapOneToManyUnidirectionalAssociationSchematic") val entityMapOneToManyUnidirectionalAssociationSchematic: Schematic
) {
    fun run(plantClassDiagram: PlantClassDiagram) {
//        log.debug("PlantClassDiagram=${plantClassDiagram.path}")
        plantClassDiagram.classes.forEach { plantClass ->
//            log.debug(". Map Class: ${plantClass.name}")
            val domainPackage = packageResolver.resolve("domain", mutableMapOf("plantClass" to plantClass))
            val ctx = mutableMapOf("plantClass" to plantClass, "domainPackage" to domainPackage)
            entityMapClassSchematic.run(fs, ctx)

            // Itera pelos atributos da classe
            plantClass.attributes.forEach { plantField ->
                ctx["plantField"] = plantField
                entityMapLongAttrSchematic.run(fs, ctx)
            }

            // Itera pelos relacionamentos da classe
            plantClass.relationships.forEach { plantRelationship ->
                ctx["plantRelationship"] = plantRelationship
                entityMapOneToManyUnidirectionalAssociationSchematic.run(fs, ctx)
            }
        }
    }
}
