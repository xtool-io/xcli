package xtool.xcli.command.converter

import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import xtool.xcli.core.model.Project
import xtool.xcli.core.model.plantuml.PlantClassDiagram

@Component
class PlantClassDiagramConverter(val project: Project) : Converter<String, PlantClassDiagram> {
    override fun convert(p0: String): PlantClassDiagram? {
        return project.plantClassDiagrams.find { it.path.fileName.toString() == p0 }
    }
}
