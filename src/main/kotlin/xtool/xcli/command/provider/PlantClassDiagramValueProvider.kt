package xtool.xcli.command.provider

import org.springframework.core.MethodParameter
import org.springframework.shell.CompletionContext
import org.springframework.shell.CompletionProposal
import org.springframework.shell.standard.ValueProviderSupport
import org.springframework.stereotype.Component
import xtool.xcli.core.model.Project

/**
 * Autocomplete para os diagramas de classe do projeto.
 */
@Component
class PlantClassDiagramValueProvider(private val project: Project) : ValueProviderSupport() {
    override fun complete(
        p0: MethodParameter?,
        p1: CompletionContext?,
        p2: Array<out String>?
    ): MutableList<CompletionProposal> {
        return project.plantClassDiagrams.asSequence()
            .map { it.path.fileName.toString() }
            .map { CompletionProposal(it) }
            .toMutableList()
    }
}
