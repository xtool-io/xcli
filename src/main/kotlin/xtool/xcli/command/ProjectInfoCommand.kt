package xtool.xcli.command

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import xtool.xcli.XTOOL_COMMAND_GROUP
import xtool.xcli.core.model.Project
import xtool.xcli.service.ProjectInfoService

/**
 * Commando para exibição de informações do projeto. (Quantidade de diagramas, interfaces de repositorio, etc)
 */
@ShellComponent
class ProjectInfoCommand(private val projectInfoService: ProjectInfoService) {

    @ShellMethod(key = ["project:info"], value = "Exibe as informações do projeto", group = XTOOL_COMMAND_GROUP)
    fun exec() {
        projectInfoService.run()
    }
}
