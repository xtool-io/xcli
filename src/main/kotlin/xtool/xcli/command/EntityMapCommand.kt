package xtool.xcli.command

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import xtool.xcli.XTOOL_COMMAND_GROUP
import xtool.xcli.command.provider.PlantClassDiagramValueProvider
import xtool.xcli.core.model.plantuml.PlantClassDiagram
import xtool.xcli.service.EntityMapService

@ShellComponent
class EntityMapCommand(val entityMapService: EntityMapService) {

    @ShellMethod(
        key = ["entity:map"],
        value = "Mapeia as clases do diagram plantuml para entidades JPA",
        group = XTOOL_COMMAND_GROUP
    )
    fun exec(
        @ShellOption(
            value = ["--from-diagram"],
            valueProvider = PlantClassDiagramValueProvider::class,
            defaultValue = "domain.plantuml"
        ) plantClassDiagram: PlantClassDiagram
    ) {
        entityMapService.run(plantClassDiagram)
    }
}
