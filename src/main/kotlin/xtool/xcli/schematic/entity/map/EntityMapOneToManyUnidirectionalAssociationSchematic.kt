package xtool.xcli.schematic.entity.map

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import xtool.xcli.core.FS
import xtool.xcli.core.ext.updateField
import xtool.xcli.core.model.plantuml.PlantClass
import xtool.xcli.core.model.plantuml.PlantRelationship
import xtool.xcli.core.schematic.Schematic

@Component
@Qualifier("EntityMapOneToManyUnidirectionalAssociationSchematic")
class EntityMapOneToManyUnidirectionalAssociationSchematic() : Schematic() {
    override fun run(fs: FS, ctx: MutableMap<String, Any>) {
        val plantClass = ctx["plantClass"] as PlantClass
        val plantRelationship = ctx["plantRelationship"] as PlantRelationship
        mapRelationship(fs, plantRelationship, plantRelationship.left, plantRelationship.right)
        mapRelationship(fs, plantRelationship, plantRelationship.right, plantRelationship.left)
    }

    private fun mapRelationship(
        fs: FS,
        plantRelationship: PlantRelationship,
        source: PlantRelationship.Linkable,
        target: PlantRelationship.Linkable
    ) {
        if (plantRelationship.isAssociation && plantRelationship.isUnidirectional) {
            if (plantRelationship.isOneToMany && target.isNavigable) {
                fs.updateClass(source.name) {
                    it.updateField(target.name)
                }
//                log.debug(".. Map Relationship: ${plantRelationship}")
            }
        }
    }
}
