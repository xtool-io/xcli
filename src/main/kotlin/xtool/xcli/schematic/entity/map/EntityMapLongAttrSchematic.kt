package xtool.xcli.schematic.entity.map

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import xtool.xcli.core.FS
import xtool.xcli.core.ext.JavaAnnotationType
import xtool.xcli.core.ext.JavaImport
import xtool.xcli.core.ext.updateAnnotation
import xtool.xcli.core.ext.updateField
import xtool.xcli.core.model.plantuml.PlantClass
import xtool.xcli.core.model.plantuml.PlantField
import xtool.xcli.core.schematic.Schematic

/**
 * Classe que executa o mapeamento de atributos do tipo Long
 */
@Component
@Qualifier("EntityMapLongAttrSchematic")
class EntityMapLongAttrSchematic() : Schematic() {
    override fun run(fs: FS, ctx: MutableMap<String, Any>) {
        val plantClass = ctx["plantClass"] as PlantClass
        val plantField = ctx["plantField"] as PlantField
        if (plantField.rawType == "Long") {
            fs.updateClass(plantClass.name) {
                it.updateField(plantField.name, plantField.javaType)
                    .setPrivate()
                    .updateAnnotation(JavaAnnotationType("Column"), JavaImport("javax.persistence.Column"))
//                log.debug(".. Map Attr: ${plantField.name}")
            }
        }
    }
}
