package xtool.xcli.schematic.entity.map

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import xtool.xcli.core.FS
import xtool.xcli.core.ext.JavaAnnotationType
import xtool.xcli.core.ext.JavaImport
import xtool.xcli.core.ext.updateAnnotation
import xtool.xcli.core.model.maven.JavaPackage
import xtool.xcli.core.model.plantuml.PlantClass
import xtool.xcli.core.schematic.Schematic

/**
 * Classe schematic que realiza o mapeamento da classe JPA. Apenas define a classe (sem atributos) e adiciona as annotations
 * correspondentes.
 */
@Component
@Qualifier("EntityMapClassSchematic")
class EntityMapClassSchematic : Schematic() {
    override fun run(fs: FS, ctx: MutableMap<String, Any>) {
        val plantClass = ctx["plantClass"] as PlantClass
        val domainPackage = ctx["domainPackage"] as JavaPackage
        fs.createClass(domainPackage) {
            it.name = plantClass.name
            it.updateAnnotation(JavaAnnotationType("Entity"), JavaImport("javax.persistence.Entity"))
            it.updateAnnotation(JavaAnnotationType("Table"), JavaImport("javax.persistence.Table"))
                .setStringValue("name", plantClass.name.toUpperCase())
            it.updateAnnotation(JavaAnnotationType("NoArgsConstructor"), JavaImport("lombok.NoArgsConstructor"))
        }
    }
}
