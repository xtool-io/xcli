package xtool.xcli.schematic

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import xtool.xcli.core.impl.FSImpl
import xtool.xcli.core.model.Project
import xtool.xcli.core.model.maven.JavaPackage
import xtool.xcli.core.model.plantuml.PlantClassDiagram
import xtool.xcli.schematic.entity.map.EntityMapClassSchematic
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class EntityMapClassSchematicTestCase {

    @BeforeEach
    fun initAll() {
        File("target/project2").deleteRecursively()
        if (!Files.exists(Paths.get("target/project2"))) {
            Files.createDirectories(Paths.get("target/project2/src/main/java/foo"))
            Files.createDirectories(Paths.get("target/project2/src/diagrams"))
            Files.copy(Paths.get("src/test/resources/project1/pom.xml"), Paths.get("target/project2/pom.xml"))
            Files.copy(
                Paths.get("src/test/resources/project1/src/diagrams/class/domain.plantuml"),
                Paths.get("target/project2/src/diagrams/domain.plantuml")
            )
        }
    }

    @Test
    fun runTest() {
        val project = Project(Paths.get("target/project2"))
        val fs = FSImpl(project);
        val entityMapCreateJpaClassSchematic = EntityMapClassSchematic()
        val plantClassDiagram = PlantClassDiagram.of(Paths.get("target/project2/src/diagrams/domain.plantuml"), project)
        val domainJavaPackage = JavaPackage("domain")
        val ctx = mutableMapOf("plantClass" to plantClassDiagram.classes.find { it.name == "PontoTransmissao" }!!, "domainPackage" to domainJavaPackage)
        entityMapCreateJpaClassSchematic.run(fs, ctx)
        Assertions.assertTrue(Files.exists(Paths.get("target/project2/app21-backend/src/main/java/br/jus/tre_pa/app/domain/PontoTransmissao.java")))
    }
}
