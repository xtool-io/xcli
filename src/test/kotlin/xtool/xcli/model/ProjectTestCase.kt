package xtool.xcli.model

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import xtool.xcli.core.model.Project
import java.nio.file.Paths

class ProjectTestCase {

    @Test
    fun getTwo_PlantClassDiagramTest() {
        val project = Project(Paths.get("src/test/resources/project1"))
        val plantClassDiagrams = project.plantClassDiagrams
        Assertions.assertEquals(2, plantClassDiagrams.size)
        println(plantClassDiagrams)
    }

    @Test
    fun getOneClass_DomainPlantClassTest() {
        val project = Project(Paths.get("src/test/resources/project1"))
        val domainDiagram = project.domainDiagram
        Assertions.assertNotNull(domainDiagram)
        Assertions.assertEquals(1, domainDiagram?.classes?.size)
        println(domainDiagram)
    }
}
