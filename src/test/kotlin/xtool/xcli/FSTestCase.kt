package xtool.xcli

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import xtool.xcli.core.impl.FSImpl
import xtool.xcli.core.model.Project
import xtool.xcli.core.model.maven.JavaPackage
import java.nio.file.Files
import java.nio.file.Paths

class FSTestCase {

    @BeforeEach
    fun initAll() {
        if (!Files.exists(Paths.get("target/project1"))) {
            Files.createDirectories(Paths.get("target/project1"))
            Files.copy(Paths.get("src/test/resources/project1/pom.xml"), Paths.get("target/project1/pom.xml"))
        }
    }

    @Test
    fun createFileTest() {
        val project = Project(Paths.get("target/"))
        val fs = FSImpl(project);
        fs.createFile("app/abc.txt", "oi")
    }

    @Test
    fun createClassTest() {
        val project = Project(Paths.get("target/project1"))
        val fs = FSImpl(project);
        fs.createClass(JavaPackage("domain"), "Ponto")
    }
}
