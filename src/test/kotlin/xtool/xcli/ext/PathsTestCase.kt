package xtool.xcli.ext

import org.junit.jupiter.api.Test
import xtool.xcli.core.ext.findFiles
import java.nio.file.Paths

class PathsTestCase {

    @Test
    fun findFilesExtTest() {
        val path = Paths.get("src/test").findFiles()
        println(path)
    }
}
